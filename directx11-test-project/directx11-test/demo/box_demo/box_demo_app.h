#pragma once

#include <application/directx_app.h>
#include <input/keyboard.h>
#include <camera/spherical_camera.h>

namespace xtest {
namespace demo {

	class BoxDemoApp : public application::DirectxApp
	{
	public:

		BoxDemoApp(HINSTANCE instance, const application::WindowSettings& windowSettings, const application::DirectxSettings& directxSettings, uint32 fps = 60);
		~BoxDemoApp();

		BoxDemoApp(BoxDemoApp&&) = delete;
		BoxDemoApp(const BoxDemoApp&) = delete;
		BoxDemoApp& operator=(BoxDemoApp&&) = delete;
		BoxDemoApp& operator=(const BoxDemoApp&) = delete;


		virtual void Init() override;
		virtual void OnResized() override;
		virtual void UpdateScene(float deltaSeconds) override;
		virtual void RenderScene() override;


	private:

		// $$$ - MR - assignment ComPtr

		 Microsoft::WRL::ComPtr<ID3D11InputLayout> m_inputLayout;
		 Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertexShader;
		 Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixelShader;
		 Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer;
		 Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer;
		 Microsoft::WRL::ComPtr<ID3D11Buffer> m_vsConstantBuffer;
		 Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_rasterizerState;

		// $$$ - MR - structs for communication with HLSL

		struct VertexIn
		{
			DirectX::XMFLOAT3 pos;
			DirectX::XMFLOAT4 color;
		};

		struct PerObjectCB {
			DirectX::XMFLOAT4X4 WVP;
		};

		// $$$ - MR - matrices
		DirectX::XMFLOAT4X4 P;
		DirectX::XMFLOAT4X4 V;
		DirectX::XMFLOAT4X4 W;
		DirectX::XMFLOAT4X4 WVP;
	};

} // demo
} // xtest

