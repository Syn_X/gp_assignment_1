#include "stdafx.h"
#include "box_demo_app.h"
#include <file/file_utils.h>
#include <math/math_utils.h>
#include <service/locator.h>


using namespace DirectX;
using namespace xtest;

using xtest::demo::BoxDemoApp;
using Microsoft::WRL::ComPtr;

BoxDemoApp::BoxDemoApp(HINSTANCE instance,
	const application::WindowSettings& windowSettings,
	const application::DirectxSettings& directxSettings,
	uint32 fps /*=60*/)
	: application::DirectxApp(instance, windowSettings, directxSettings, fps)
{}


BoxDemoApp::~BoxDemoApp()
{}


void BoxDemoApp::Init()
{
	application::DirectxApp::Init();

	//TODO: put your init code here (creation of shaders and others...)
	D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexIn, color), D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	std::future<file::BinaryFile> vsByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\box_demo_VS.cso"));
	file::BinaryFile vsByteCode = vsByteCodeFuture.get();

	XTEST_D3D_CHECK(m_d3dDevice->CreateInputLayout(vertexDesc, 2, vsByteCode.Data(), vsByteCode.ByteSize(), &m_inputLayout));

	XTEST_D3D_CHECK(m_d3dDevice->CreateVertexShader(vsByteCode.Data(), vsByteCode.ByteSize(), nullptr, &m_vertexShader));

	std::future<file::BinaryFile> psByteCodeFuture = file::ReadBinaryFile(std::wstring(GetRootDir()).append(L"\\box_demo_PS.cso"));
	file::BinaryFile psByteCode = psByteCodeFuture.get();

	XTEST_D3D_CHECK(m_d3dDevice->CreatePixelShader(psByteCode.Data(), psByteCode.ByteSize(), nullptr, &m_pixelShader));


	// Vertex buffer

	VertexIn vertices[] =
	{
		{ XMFLOAT3(+1.f, +1.f, +1.f), XMFLOAT4(DirectX::Colors::Red) },
		{ XMFLOAT3(+1.f, +1.f, -1.f), XMFLOAT4(DirectX::Colors::Red) },
		{ XMFLOAT3(+1.f, -1.f, +1.f), XMFLOAT4(DirectX::Colors::Yellow) },
		{ XMFLOAT3(+1.f, -1.f, -1.f), XMFLOAT4(DirectX::Colors::Yellow) },
		{ XMFLOAT3(-1.f, +1.f, +1.f), XMFLOAT4(DirectX::Colors::Blue) },
		{ XMFLOAT3(-1.f, +1.f, -1.f), XMFLOAT4(DirectX::Colors::Blue) },
		{ XMFLOAT3(-1.f, -1.f, +1.f), XMFLOAT4(DirectX::Colors::Green) },
		{ XMFLOAT3(-1.f, -1.f, -1.f), XMFLOAT4(DirectX::Colors::Green) }
	};


	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = vertices;

	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &m_vertexBuffer));


	// Index buffer

	uint32 indicies[] =
	{
		//back
		0, 6, 2,
		0, 4, 6,
		//top
		0, 1, 5,
		0, 5, 4,
		//front
		1, 3, 7,
		1, 7, 5,
		//right
		0, 2, 3,
		0, 3, 1,
		//left
		6, 4, 5,
		6, 5, 7,
		//bottom
		3, 2, 6,
		3, 6, 7,
	};

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.ByteWidth = sizeof(indicies);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA indexInitData;
	indexInitData.pSysMem = indicies;

	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&indexBufferDesc, &indexInitData, &m_indexBuffer));


	// Constant buffer

	D3D11_BUFFER_DESC vsConstantBufferDesc;
	vsConstantBufferDesc.ByteWidth = sizeof(PerObjectCB);
	vsConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vsConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	vsConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vsConstantBufferDesc.MiscFlags = 0;
	vsConstantBufferDesc.StructureByteStride = 0;

	XTEST_D3D_CHECK(m_d3dDevice->CreateBuffer(&vsConstantBufferDesc, nullptr, &m_vsConstantBuffer));

	
	// Rasterizer state

	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	XTEST_D3D_CHECK(m_d3dDevice->CreateRasterizerState(&rasterizerDesc, &m_rasterizerState));

	// Init prospection matrix
	XMStoreFloat4x4(&P, XMMatrixPerspectiveFovLH(XMConvertToRadians(45.f), AspectRatio(), 0.01f, 100.f));

	// Init view matrix
	XMVECTOR eyePosition = XMVectorSet(0, 0, -10, 1);
	XMVECTOR focusPoint = XMVectorSet(0, 0, 0, 1);
	XMVECTOR upDirection = XMVectorSet(0, 1, 0, 0);
	XMStoreFloat4x4(&V, XMMatrixLookAtLH(eyePosition, focusPoint, upDirection));

}


void BoxDemoApp::OnResized()
{
	application::DirectxApp::OnResized();

	//TODO: when the window is resized update your projection matrix in order to avoid any distortion in the image

	XMStoreFloat4x4(&P, XMMatrixPerspectiveFovLH(XMConvertToRadians(45.f), AspectRatio(), 0.01f, 100.f));

}



void BoxDemoApp::UpdateScene(float deltaSeconds)
{
	//TODO: update all your data e.g. matrices
	
	// Create WVP
	
	static float angle = 0.0f;
	angle += 45.f * deltaSeconds;
	XMVECTOR rotationAxis = XMVectorSet(0, 1, 1, 0);
	XMStoreFloat4x4(&W, XMMatrixRotationAxis(rotationAxis, XMConvertToRadians(angle)));
	
	XMStoreFloat4x4(&WVP, XMMatrixTranspose(XMLoadFloat4x4(&W) * XMLoadFloat4x4(&V) * XMLoadFloat4x4(&P)));
	   
	// Map constant buffer to disable GPU access
	D3D11_MAPPED_SUBRESOURCE   mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	XTEST_D3D_CHECK(m_d3dContext->Map(m_vsConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	PerObjectCB* constantBufferData = static_cast<PerObjectCB*>(mappedResource.pData);

	// Update WVP
	XMStoreFloat4x4(&constantBufferData->WVP, XMLoadFloat4x4(&WVP));

	// Unmap constant buffer to enable GPU access
	m_d3dContext->Unmap(m_vsConstantBuffer.Get(), 0);

} 


void BoxDemoApp::RenderScene()
{
	//TODO: render your scene and present the frame  

	// Clear buffers
	m_d3dContext->ClearDepthStencilView(m_depthBufferView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
	m_d3dContext->ClearRenderTargetView(m_backBufferView.Get(), DirectX::Colors::Black);

	// Binding shaders
	m_d3dContext->RSSetState(m_rasterizerState.Get());
	m_d3dContext->IASetInputLayout(m_inputLayout.Get());
	m_d3dContext->VSSetShader(m_vertexShader.Get(), nullptr, 0);
	m_d3dContext->PSSetShader(m_pixelShader.Get(), nullptr, 0);

	// Binding constant buffer
	UINT bufferRegister = 0;
	m_d3dContext->VSSetConstantBuffers(bufferRegister, 1, m_vsConstantBuffer.GetAddressOf());

	// Binding the mesh
	UINT stride = sizeof(VertexIn);
	UINT offset = 0;
	m_d3dContext->IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &stride, &offset);
	m_d3dContext->IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	m_d3dContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); 
	
	// Let's f***ing draw!
	m_d3dContext->DrawIndexed(36, 0, 0);
	
	XTEST_D3D_CHECK(m_swapChain->Present(0, 0));
}

